<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH.'/libraries/REST_Controller.php');
define('RUTA_LOGX',					'/sysx/progs/afore/log/constanciaafiliacion');

    /**
     * Constructor , controlador con Autorizacion por token
     */

class CtrlEjemplo extends REST_Controller {


    public function __construct()
    {
		$method = $_SERVER['REQUEST_METHOD'];

		if($method == "OPTIONS") {
			die();
		}
		parent::__construct();

        /**  configuracion que permite:
                * Tipo de peticion
                * Autorizacion a través de token.   
        */
		$this->_APIConfig([
			'methods' => ['POST'],
			'requireAuthorization' => true,
		]);
		// se carga el modelo del registro de tokens
        $this->load->model("MdlAuth");
        
        // verifica si el token se encuentra registro en la base de datos 
		if (!$this->MdlAuth->verificarSesionToken(getallheaders()["Authorization"])) {
			$this->response( [
				"status" => 0,
				"result" => "El promotor no ha iniciado sesión"
			],
			200);
		}
        // Se carga los modelos que se utilizaran en los controladores 
        $this->load->model('MdlEjemplo');
      
    }

    /**
     * Ejemplo de una petición  
     */

    public function consultacatalogoestadocivil_post()
    {

    	try {

    		$response = $this->MdlEjemplo->consultacatalogoestadocivil();

			if($response)
			{
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($response as $reg)
				{
					$arrDatos['registros'][] = $reg;
				}
			}
    	}
    	catch (Exception $mensaje)
    	{
    		$arrDatos['estatus'] = -1;
			  $arrDatos['descripcion'] = $mensaje;
			  $arrDatos['registros'] = null;
		  }

		$this->response($arrDatos, REST_Controller::HTTP_OK);
    }

}
