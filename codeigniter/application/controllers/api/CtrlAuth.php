<?php

if (!defined("BASEPATH")) exit("No direct script access allowed");
//include Rest Controller library
require(APPPATH . "/libraries/REST_Controller.php");

/**
 * Registro y eliminacion de token
 */
class CtrlAuth extends REST_Controller
{
    public function __construct()
    {  // API Configuration
        parent::__construct();
        $this->load->model("MdlAuth");
    }
    /**
     * Registro de token
     *
     * @link [api/user/login]
     * @method POST
     * @return Response|void
     */
    public function RegistroToken_post()
    {
        
        $data = [
            "idEmpleado" => $this->post("idEmpleado"),
            "codigoEmpleado" => $this->post("codigoEmpleado"),
            "data" => date("Y-m-d h:i:s a"),
        ];

        // Cargar biblioteca de autorizaciones o cargar en el archivo 
        // de configuración de carga automática
        $this->load->library("Authorization_Token");

        // generar  token
        $token = $this->authorization_token->generateToken($data);
 

        try {

            $respuesta = $this->MdlAuth->guardarRegistro($data["codigoEmpleado"], $token);
         
            if ($respuesta == 1) {
                $this->response(
                    [
                        "status" => true,
                        "result" => [
                            "codigoEmpleado" => $this->post("codigoEmpleado"),
                            "token" => $token,
                        ],

                    ],
                    200
                );
            } else {
                $this->response(
                    [
                        "status" => false,
                        "result" => [
                            "token" => "Intente de nuevo",
                        ],
                    ],
                    400
                );
            }
        } catch (Exception $mensaje) {
            $this->response(
                [
                    "status" => false,
                    "result" => [
                        "token" => $mensaje,
                    ],

                ],
                500
            );
        }
    }
    /**
     * Cerrar sesion
     * @method POST
     * @return Response|void
     */
    public function eliminarToken_post()
    {



        try {
            $data = [
               
                "codigoEmpleado" => $this->post("codigoEmpleado"),
                
            ];

            $respuesta =  $this->MdlAuth->eliminarToken($data["codigoEmpleado"]);

            if ($respuesta) {
                $this->response(
                    [
                        "status" => 1,
                        "result" => $respuesta
                    ],
                    200
                );
            } else {
                $this->response(
                    [
                        "status" => 0,
                        "result" =>  "Intente de nuevo",
                    ],
                    400
                );
            }
        } catch (Exception $mensaje) {
            $this->response(
                [
                    "status" => 0,
                    "result" => $mensaje,
                ],
                500
            );
        }
    }
}
