<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MdlEjemplo extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        //load database library
        $this->Pgaforeglobal = $this->load->database("Pgaforeglobal", true);
    }


    function consultacatalogoestadocivil()
    {

        $query = $this->Pgaforeglobal->query("select clavec, trim(desccomp) as desccomp from fn_catestadocivil()");
        return $query->result();
    }
}
